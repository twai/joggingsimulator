#pragma once
#include <chrono>

class Timer{
public:
    Timer();
    ~Timer();
    void Start();
    double Restart();
    void Resume();
    double Stop();
    double Get() const;
    void Clear();
    void ResetIfPassed(double seconds);
private:
    std::chrono::high_resolution_clock::time_point start_time_;
    double elapsed_ = 0.0;
    bool running_ = false;

};

