#pragma once
#include <SFML/Graphics.hpp>
#include "AssetManager.h"
#include "EntityBank.h"


// Asset manager, component bank, etc. go here
struct Context {
    AssetManager* assetManager;
    EntityBank* entityBank;
    sf::RenderWindow* window;
    sf::Vector2f globalScale;
    sf::Vector2i tileDimensions;
    int tileSize;
    float scrollSpeed;
    float originalScrollSpeed;
    float dt;
    float constantG;
    int instance;
    int charges = 0;
    bool active = false;
    bool hardMode = false;
    float blindsFactor = 0.0f;
    bool heroDied = false;
    sf::Sprite* blindsSprite = nullptr;
    int nextHeroOut = 0;
    int nextHeroIn = 0;
    int spawnBias = 0;
    long long metersTraveled = 0;
};
