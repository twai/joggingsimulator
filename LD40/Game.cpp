#include "Game.h"
#include <Windows.h>
#include "SoundBank.h"

Game::Game()
{
}


Game::~Game()
{
    for (auto g : games_) {
        if (g) {
            delete g;
        }
    }
}

bool Game::Initialize()
{
    if (!SoundBank::Initialize()) {
        std::cerr << "Failed to initialize sound bank." << std::endl;
        return false;
    }
    if (!GuiManager::Initialize()) {
        std::cerr << "Failed to load fonts. " << std::endl;
        return false;
    }

    scoreText_ = GuiManager::GetText();
    scoreText_->setString("Distance Traveled\n 0");
    scoreText_->setCharacterSize(24);
    scoreText_->setFillColor(sf::Color::White);
    scoreText_->setOutlineColor(sf::Color::Black);
    scoreText_->setOutlineThickness(1.0f);
    scoreText_->setPosition(sf::Vector2f(900, 10));

    menuText_ = GuiManager::GetText();
    menuText_->setString("Controls: A, S, D and F to jump.\n   Press enter to start!");
    menuText_->setCharacterSize(52);
    menuText_->setFillColor(sf::Color::White);
    menuText_->setOutlineColor(sf::Color::Black);
    menuText_->setOutlineThickness(5.0f);
    menuText_->setPosition(sf::Vector2f(100, 300));

    gameOverText_ = GuiManager::GetText();
    gameOverText_->setString("Press enter to try again!");
    gameOverText_->setCharacterSize(52);
    gameOverText_->setFillColor(sf::Color::White);
    gameOverText_->setOutlineColor(sf::Color::Black);
    gameOverText_->setOutlineThickness(5.0f);
    gameOverText_->setPosition(sf::Vector2f(150, 300));
    gameOverText_->setScale(sf::Vector2f(0, 0));



    srand(time(NULL));
    // Adjust window size to be a multiple of global scale
    sf::Vector2i tileDim;
    tileDim.x = 1280 / (GLOBAL_SCALE * TILE_SIZE);
    tileDim.y = 4;
    window_w_ = tileDim.x * (GLOBAL_SCALE * TILE_SIZE);
    window_h_ = tileDim.y * (GLOBAL_SCALE * TILE_SIZE) * 4;
    // Create window
    sf::ContextSettings s;
    s.attributeFlags;
    window_.create(sf::VideoMode(window_w_, window_h_), "Hello World!", sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize);


    window_.setFramerateLimit(120);

    // Initialize asset manager
    if (!assetManager_.Initialize()) {
        std::cerr << "Failed to initialize asset manager." << std::endl;
        return false;
    }   
    renderer_.Initialize(&window_, assetManager_);
    timer_.Start();

    DelayedInitialize();
    return true;
}



void Game::DelayedInitialize()
{
    sf::Vector2i tileDim;
    tileDim.x = 1280 / (GLOBAL_SCALE * TILE_SIZE);
    tileDim.y = 4;

    bool isHardMode = numGames > 4;
    float memallocMb = 0;
    for (int i = 0; i < numGames; i++) {
        bool first = i == 0;

        games_.push_back(new GameInstance());
        GameInstance* g = games_.back();
        memallocMb += g->entityBank_.MbUsed;
        g->context_.assetManager = &assetManager_;
        g->context_.window = &window_;
        g->context_.entityBank = &g->entityBank_;
        g->context_.globalScale = sf::Vector2f(GLOBAL_SCALE, GLOBAL_SCALE);
        g->context_.tileDimensions = tileDim;
        g->context_.tileSize = TILE_SIZE;
        g->context_.originalScrollSpeed = -100;
        g->context_.scrollSpeed = first ? -100 : 0;
        g->context_.constantG = 9.82 * 200;
        g->context_.instance = i;
        g->context_.active = first ? true : false;
        g->context_.heroDied = first ? false : true;
        g->context_.blindsFactor = first ? 0.0f : 0.99f;
        g->context_.hardMode = isHardMode;
        g->context_.spawnBias = 100;
        g->systems_.Initialize(g->context_);

        std::cout << "ECS is using " << memallocMb << "Mb" << std::endl;
    }
}

bool Game::Update()
{
    // Poll events, return false if the window is closed
    if (window_.isOpen()) {
        sf::Event event;
        while (window_.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                return false;
            }
            if (event.type == sf::Event::Resized) {
                event.size.width;
                event.size.height;

            }
        }
    }

    if (window_.hasFocus()) {
        if (!init_ && sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
            init_ = true;
            menuText_->setScale(sf::Vector2f(0, 0));
        }
        if (!init_) {
            return true;
        }


        // Increase window size
        bool pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
        if (pressed && !prevUp_) {
            renderer_.ScaleUp(&window_);
        }
        prevUp_ = pressed;

        // Decrease window size
        pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
        if (pressed && !prevDown_) {
            renderer_.ScaleDown(&window_);
        }
        prevDown_ = pressed;

        // Start new instance (tmp)
        bool playing = false;
        for (int i = 0; i < numGames; i++) {
            if (!games_[i]->context_.heroDied) {
                playing = true;
            }
        }

        // If all our heroes died
        if (!playing) {
            gameOverText_->setScale(sf::Vector2f(1, 1));
            for (int i = 0; i < 4; i++) {
                if (XboxController::isButtonPressed(i, XboxController::XboxButton::START)
                    || sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
                    gameOverText_->setScale(sf::Vector2f(0, 0));
                    games_[0]->context_.active = true;
                    games_[0]->context_.nextHeroIn = 0;
                    games_[0]->systems_.Reset(games_[0]->context_);
                    for (int j = 0; j < numGames; j++) {
                        games_[j]->context_.metersTraveled = 0;
                    }
                    break;
                }
            }
        }
        double t = timer_.Restart();

        float dt = (float)t;

        std::string title = "Suburb Jogging Simulator ";
        window_.setTitle(title);
        long long totalMetersTraveled = 0;
        bool resetSpawnBias = false;
        // Check in on all of our games
        for (int i = 0; i < numGames; i++) {

            // Deal with starting new rounds
            if (games_[i]->context_.charges > 0) {
                games_[i]->context_.charges = 0;
                for (int j = 0; j < numGames; j++) {
                    if (games_[j]->context_.heroDied) {
                        games_[j]->context_.active = true;
                        games_[j]->context_.nextHeroIn = games_[i]->context_.nextHeroOut;
                        games_[j]->systems_.Reset(games_[j]->context_);
                        break;
                    }
                }
                // If we reach this place, all instances are full
                // Start counting towards overdrive..?
            }

            // spawn bias
            if (games_[i]->context_.spawnBias == 0) {
                resetSpawnBias = true;
            }

            // Score
            totalMetersTraveled += games_[i]->context_.metersTraveled;
            scoreText_->setString("Distance Traveled\n " + std::to_string(totalMetersTraveled) + "m");

        }
        if (resetSpawnBias) {
            for (int i = 0; i < numGames; i++) {
                games_[i]->context_.spawnBias = 1;
            }
        }


        // Make sure we don't start writing outside of our data
        for (int i = 0; i < numGames; i++) {
            if (games_[i]->entityBank_.CapBreached()) {
                ErrorState = true;
                return false;
            }
        }

       
        if (dt > 0.1) {
            dt = 0.1;
        }

        for (int i = 0; i < numGames; i++) {
            games_[i]->context_.dt = dt;
            games_[i]->systems_.Run(games_[i]->context_);
        }
    }
    return true;

}

void Game::Render()
{
    renderer_.Clear(&window_);
    for (int i = 0; i < numGames; i++) {
        renderer_.Run(games_[i]->context_);
    }
    GuiManager::Draw(window_, renderer_.GetFinalScale());
    renderer_.Present(&window_);
}
