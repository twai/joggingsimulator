#pragma once
#include "Game.h"


int main() {
    {
        Game game;
        if (game.Initialize()) {
            bool run = game.Update();
            while (run) {
                game.Render();
                run = game.Update();
            }
        }
        else {
            std::cout << "Failed to initialize... (press enter to exit)" << std::endl;
            std::cin.get();
        }

        if (game.ErrorState) {
            std::cout << "The game encountered a fatal error... (press enter to exit)" << std::endl;
            std::cin.get();
        }
    }
    return 0;
}