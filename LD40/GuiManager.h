#pragma once
#include "AssetManager.h"
#include <vector>

class GuiManager
{
public:
    static bool Initialize();
    static sf::Text* GetText();
    static sf::Font* GetFont();
    static void Draw(sf::RenderWindow& window, float finalScale);
    
    GuiManager();
    ~GuiManager();
private:
    static sf::Font font_;
    static std::vector<sf::Text> texts_;
};

