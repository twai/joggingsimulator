#pragma once
#include "System.h"
#include <SFML/System/Vector2.hpp>
namespace sf {
    class RenderWindow;
}

class RenderSystem{
public:
    RenderSystem();
    ~RenderSystem();
    void Clear(sf::RenderWindow* window);
    void Initialize(sf::RenderWindow* window, AssetManager& assetManager);
    void Run(Context& ctx);
    void Present(sf::RenderWindow* window);
    void ScaleUp(sf::RenderWindow* window);
    void ScaleDown(sf::RenderWindow* window);   
    float GetFinalScale() const { return finalScale_; }
private:
    float finalScale_ = 1.0f;
    sf::Vector2u initialSize_;
};
