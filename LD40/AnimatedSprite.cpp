#include "AnimatedSprite.h"



AnimatedSprite::AnimatedSprite()
{
}


AnimatedSprite::~AnimatedSprite()
{
}

void AnimatedSprite::AddSprite(sf::Sprite* sprite)
{
    sprites_.push_back(sprite);
}

void AnimatedSprite::SetAnimation(AnimationName name, int firstIndex, int lastIndex, float timePerFrame)
{
    if (firstIndex >= 0 && lastIndex < sprites_.size()) {
        animations_[name] = Animation(firstIndex, lastIndex, timePerFrame);
    }
}

void AnimatedSprite::SetTime(AnimationName name, float time)
{
    auto it = animations_.find(name);
    if (it != animations_.end()) {
        it->second.CurrentTime = time;
        it->second.CurrentIndex = static_cast<int>(it->second.CurrentTime / it->second.TimePerFrame);
    }
}

void AnimatedSprite::Tick(AnimationName name, float time)
{
    auto it = animations_.find(name);
    if (it != animations_.end()) {
        it->second.CurrentTime += time;
        while (it->second.CurrentTime > it->second.TotalTime) {
            it->second.CurrentTime -= it->second.TotalTime;
        }
        it->second.CurrentIndex = it->second.FirstIndex + static_cast<int>(it->second.CurrentTime / it->second.TimePerFrame);
    }
}

sf::Sprite * AnimatedSprite::GetSprite(AnimationName name)
{
    auto it = animations_.find(name);
    if (it != animations_.end()) {
        return sprites_.at(it->second.CurrentIndex);
    }
    else {
        return AssetManager::EmptySprite();
    }

}
