#include "EntityBank.h"
#include <iostream>

const double EntityBank::MbUsed = ((sizeof(ComponentMask) + sizeof(Cmp::Parent) + sizeof(Cmp::Position) + sizeof(Cmp::Sprite)
    + sizeof(Cmp::Input) + sizeof(Cmp::Speed) + sizeof(Cmp::Acceleration) + sizeof(Cmp::ScrollingTile) + sizeof(Cmp::HitBox)) * MAX_ENTITIES) / 1000;

EntityBank::EntityBank()
{
    entities_[0] = ComponentFlag::InUse;
}


EntityBank::~EntityBank()
{
    std::cout << "Highest entity: " << highest_ << std::endl;
}

Entity EntityBank::CreateEntity(ComponentMask components)
{
    // Find a free entity
    for (Entity e = 0; e < MAX_ENTITIES; e++) {
        if (entities_[e] == ComponentFlag::None) { // Found one!
            entities_[e] |= components;
            if (e > highest_) {
                highest_ = e;
            }
            return e;
        }
    }

    std::cerr << "Too many entities!" << std::endl;
    capBreached_ = true;
    return NO_ENTITY;
}

void EntityBank::DestroyEntity(Entity e)
{
    // Small safeguard in case I accidentally try to destroy NO_ENTITY
    if (e == NO_ENTITY) {
        return;
    }

    // Reset all components so they're fresh for the next user!
    if (HasComponents(e, ComponentFlag::Sprite)) {
        Sprites[e].handle_ = 0;
        Sprites[e].layer_ = 0;
        Sprites[e].sprite_ = nullptr;
    }

    if (HasComponents(e, ComponentFlag::Position)) {
        Positions[e].position_ = sf::Vector2f();
    }

    // Finally, clear the entity for use
    entities_[e] = ComponentFlag::None;
}

bool EntityBank::HasComponents(Entity e, ComponentMask components)
{
   return (entities_[e] & components) == components;
}

void EntityBank::AddComponents(Entity entity, ComponentMask components)
{
    entities_[entity] |= components;
}

void EntityBank::RemoveComponents(Entity entity, ComponentMask components)
{
    entities_[entity] &= ~components;
}
