#include "Strings.h"

const std::string Strings::SpritesheetPath = "assets/spritesheet.png";
const std::string Strings::CharSpritesheetPath = "assets/running_spritesheet.png";
const std::string Strings::GUIBorderPath = "assets/frame.png";
const std::string Strings::GUIBlindsPath = "assets/blinds.png";
const std::string Strings::SoundJumpPath = "assets/jump2.wav";
const std::string Strings::SoundRecruitPath = "assets/recruit2.wav";
const std::string Strings::SoundDeathPath = "assets/death.wav";