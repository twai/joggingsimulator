#include "GuiManager.h"
#include "Strings.h"

sf::Font GuiManager::font_;
std::vector<sf::Text> GuiManager::texts_;



GuiManager::GuiManager() {

}

GuiManager::~GuiManager()
{
}

bool GuiManager::Initialize()
{
    texts_.reserve(100);
    if (!font_.loadFromFile("Fonts/Hesitation.ttf")) {
        return false;
    }
    return true;
}

sf::Text* GuiManager::GetText()
{
    texts_.push_back(sf::Text());
    sf::Text* ref = &texts_.back();
    ref->setFont(font_);
    return ref;
}

sf::Font * GuiManager::GetFont()
{
    return &font_;
}

void GuiManager::Draw(sf::RenderWindow & window, float finalScale)
{
    // Reset the view
    sf::Vector2u wSize = window.getSize();
    sf::View view(sf::FloatRect(0, 0, wSize.x, wSize.y));
    view.setViewport(sf::FloatRect(0, 0, 1, 1));
    window.setView(view);

    sf::Transform t;
    t.scale(sf::Vector2f(finalScale, finalScale));

    // And draw
    for (sf::Text& text : texts_) {
        window.draw(text, t);
    }
}
