#pragma once
#include <stdint.h>
#include <SFML/Graphics.hpp>
#include "AssetManager.h"
#include "AnimatedSprite.h"

using Entity = int;
using ComponentMask = int;

enum ComponentFlag {
    None = 0,        // Empty and available
    InUse = 1 << 0,  // can be empty, but reserved
    Sprite = 1 << 1,
    Physics = 1 << 2,
    Position = 1 << 3,
    Animation = 1 << 4,
    Input = 1 << 5,
    Scrolling = 1 << 6,
    Hero = 1 << 7,
    Speed = 1 << 8,
    Acceleration = 1 << 9,
    WorldEntity = 1 << 10,  // Assign to anything that exists in the world. (to separate from UI, etc)
    HitBox = 1 << 11,
    SilentHitBox = 1 << 12, // Non blocking
};

namespace Cmp{
    struct Sprite {
        SpriteHandle handle_;
        sf::Sprite* sprite_;
        int layer_;
        const char* debugName_ = "NONE";
    };

    struct Animation{
        AnimatedSprite* animation_;
        int layer_;
    };

    struct Position {
        sf::Vector2f position_;
    };

    struct Parent {
        Entity parent_;
    };

    struct Input {
        int controllerIndex_;
        int assignedKey_;
    };

    struct Speed {
        sf::Vector2f speed_;
    };

    struct Acceleration {
        sf::Vector2f acceleration_;
        bool jumping_ = true;
    };

    struct ScrollingTile {
        enum TileType {
            Sky, Ground, Prop
        };
        TileType type_;
    };

    struct HitBox {
        sf::FloatRect hitBox_;
    };


    struct SilentHitBox {
        enum Usage {
            Hero,
            HeroMarker,
        };
        sf::FloatRect hitBox_;
        Usage usage_;
        int index_;
    };
};