#include "RenderSystem.h"
#include <utility>
#include <vector>
#include <SFML/Graphics.hpp>
#include "Components.h"
#include "Strings.h"

RenderSystem::RenderSystem(){
}


RenderSystem::~RenderSystem(){
}

void RenderSystem::Clear(sf::RenderWindow* window)
{
    window->clear(sf::Color(197, 224, 220));
}

void RenderSystem::Initialize(sf::RenderWindow* window, AssetManager& a)
{
    initialSize_ = window->getSize();
}

void RenderSystem::Run(Context & ctx)
{
    if (!ctx.blindsSprite) {
        AssetManager* a = ctx.assetManager;
        a->LoadTexture(Strings::GUIBlindsPath);
        SpriteHandle h = a->CreateSprite(Strings::GUIBlindsPath);
        ctx.blindsSprite = a->GetSprite(h);
        //  sf::Color insepctMe = blindsSprite_->getColor();
        ctx.blindsSprite->setColor(sf::Color(255, 255, 255, 0));
    }


    if (ctx.blindsFactor < 1.0f) {
        if (ctx.heroDied) {
        ctx.blindsFactor += ctx.dt * 0.2f;
        int alpha = 255 * ctx.blindsFactor;
        alpha = std::min(alpha, 255);
        ctx.blindsSprite->setColor(sf::Color(255, 255, 255, alpha));
        }
    }
    else if(ctx.heroDied) {
        ctx.active = false;
    }


    if(ctx.blindsFactor > 0.0f && !ctx.heroDied){
        ctx.blindsFactor = 0.0f;
        ctx.blindsSprite->setColor(sf::Color(255, 255, 255, 0));
    }


    float yOffset = ctx.instance * 0.25;
    float xOffset = 0.0f;
    float height = ctx.tileDimensions.y * ctx.tileSize * ctx.globalScale.y;
    height = ctx.window->getSize().y / 4;
    float width = ctx.window->getSize().x;
    float vpWidth = 1.0f;
    // EIGHT WINDOWS???!?!?!?!
    if (ctx.instance > 3) {
        yOffset -= 1.0f;
        xOffset = 0.5f;
        vpWidth *= 0.5f;
    }

    sf::View view(sf::FloatRect(0, 0, vpWidth * width, height));
    view.setViewport(sf::FloatRect(xOffset, yOffset, vpWidth, 0.25));


    sf::RenderWindow* w = ctx.window;
    w->setView(view);


    // Draw entities
    for (int i = 0; i < 5; i++) {
        for (Entity e = 0; e < MAX_ENTITIES; e++) {
            if (ctx.entityBank->HasComponents(e, ComponentFlag::Sprite | ComponentFlag::Position)) {
                if (ctx.entityBank->Sprites[e].layer_ == i) {
                    sf::Transform transform;
                    transform.scale(sf::Vector2f(finalScale_, finalScale_));
                    transform.scale(ctx.globalScale);
                    transform.translate(ctx.entityBank->Positions[e].position_);
                    w->draw(*ctx.entityBank->Sprites[e].sprite_, transform);
                }
            }
            else if (ctx.entityBank->HasComponents(e, ComponentFlag::Sprite | ComponentFlag::Animation)) {
                if (ctx.entityBank->Sprites[e].layer_ == i) {
                    sf::Transform transform;
                    transform.scale(sf::Vector2f(finalScale_, finalScale_));
                    transform.scale(ctx.globalScale);
                    transform.translate(ctx.entityBank->Positions[e].position_);
                    w->draw(*ctx.entityBank->Sprites[e].sprite_, transform);
                }
            }
        }
    }


    float blindsScaleX = 2.5f * vpWidth;
    float blindScaleY = 0.375f;
    sf::Transform blindsTransform;
    blindsTransform.scale(sf::Vector2f(finalScale_, finalScale_));
    blindsTransform.scale(blindsScaleX, blindScaleY);
    //blindsTransform.scale(sf::Vector2f(finalScale_, finalScale_));
    w->draw(*ctx.blindsSprite, blindsTransform);

    // Draw border
    float borderWidth = ctx.hardMode ? 0.5f : 1.0f;
    sf::Transform t;
    t.scale(sf::Vector2f(finalScale_, finalScale_));
    t.scale(sf::Vector2f(borderWidth, 1));
    w->draw(*AssetManager::BorderSprite(), t);
}

void RenderSystem::Present(sf::RenderWindow* window)
{
    window->display();
}

void RenderSystem::ScaleUp(sf::RenderWindow* window)
{
    finalScale_ += .2;
    if (finalScale_ < 0) { finalScale_ = 0; }
    sf::Vector2u size;
    size.x = initialSize_.x * finalScale_;
    size.y = initialSize_.y * finalScale_;
    window->setSize(size);
}

void RenderSystem::ScaleDown(sf::RenderWindow* window)
{
    finalScale_ -= .2;
    if (finalScale_ < 0) { finalScale_ = 0; }
    sf::Vector2u size;
    size.x = initialSize_.x * finalScale_;
    size.y = initialSize_.y * finalScale_;
    window->setSize(size);
}