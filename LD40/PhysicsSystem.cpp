#include "PhysicsSystem.h"
#include <iostream>
#include <math.h>
#define MAX_ACCELERATION 1000

PhysicsSystem::PhysicsSystem(){
}


PhysicsSystem::~PhysicsSystem(){
}

void PhysicsSystem::Initialize(Context & ctx)
{
}

void PhysicsSystem::Run(Context & ctx)
{
    if (!ctx.active) {
        return;
    }
    ctx.scrollSpeed -= ctx.dt;
    // Make sure to only update in full pixel steps
    accumulated_dt_ += ctx.dt;
   float minimum_dt = 1.0f / (ctx.globalScale.x * abs(ctx.scrollSpeed));
   if (minimum_dt > 0.05f) {
       // Something is off, we don't want to break our physics by taking huge timesteps
       minimum_dt = 0.0033333334;
   }
   // float minimum_dt = 1.0f / (100 * ctx.globalScale.x);
    //std::cout << "Minimum dt: " << minimum_dt << std::endl;
    float dt = 0.0f;
    int steps = 0;
    while (accumulated_dt_ > minimum_dt) {
        accumulated_dt_ -= minimum_dt;
        dt += minimum_dt;
        steps++;
    }
    //if (steps > 0) {
    //    std::cout << "Steps: " << steps;
    //    std::cout << " Movement: " << ctx.scrollSpeed * dt << std::endl;
    //}

    // Move all world entities with the scroll speed
    ComponentMask components = ComponentFlag::Position | ComponentFlag::WorldEntity;
    for (Entity e = 0; e < MAX_ENTITIES; e++) {
        if (ctx.entityBank->HasComponents(e, components)) {
            ctx.entityBank->Positions[e].position_.x += ctx.scrollSpeed * dt;
        }
    }

    // Update accelerations
    components = ComponentFlag::Acceleration | ComponentFlag::Speed;
    for (Entity e = 0; e < MAX_ENTITIES; e++) {
        if (e == 1) {
            int brk = 12;
        }
        if (ctx.entityBank->HasComponents(e, components)) {
            if (ctx.entityBank->Accelerations[e].jumping_) {
                ctx.entityBank->Accelerations[e].acceleration_.y -= ctx.constantG * dt;
                if (ctx.entityBank->Accelerations[e].acceleration_.y > MAX_ACCELERATION) {
                    ctx.entityBank->Accelerations[e].acceleration_.y = MAX_ACCELERATION;
                }
                else if (ctx.entityBank->Accelerations[e].acceleration_.y < -MAX_ACCELERATION) {
                    ctx.entityBank->Accelerations[e].acceleration_.y = -MAX_ACCELERATION;
                }
            }
            ctx.entityBank->Speeds[e].speed_ -= ctx.entityBank->Accelerations[e].acceleration_ * dt;
            //ctx.entityBank->Speeds[e].speed_.y += 20 * ctx.dt;
        }
    }

    // Update speed based movement
    components = ComponentFlag::Speed | ComponentFlag::Position;
    for (Entity e = 0; e < MAX_ENTITIES; e++) {
        if (e == 1) {
            int brk = 12;
        }
        if (ctx.entityBank->HasComponents(e, components)) {
            if (ctx.entityBank->HasComponents(e, ComponentFlag::HitBox)) {
                sf::Vector2f dm = ctx.entityBank->Speeds[e].speed_ * dt;
                auto hits = CheckCollisions(ctx, e, dm);
                if (hits.first) { // x
                    ctx.entityBank->Speeds[e].speed_.x = 0.0f;
                    ctx.entityBank->Accelerations[e].acceleration_.x = 0.0f;
                }
                else {
                    ctx.entityBank->Positions[e].position_.x += ctx.entityBank->Speeds[e].speed_.x * dt;
                }


                if (hits.second) { // y
                    //if (ctx.entityBank->Speeds[e].speed_.y > 0) { // Ugly workaround for jumping
                        ctx.entityBank->Speeds[e].speed_.y = 0.0f;
                        ctx.entityBank->Accelerations[e].acceleration_.y = 0.0f;
                        ctx.entityBank->Accelerations[e].jumping_ = false;
                   // }
                }
                else if(ctx.entityBank->Accelerations[e].jumping_ == false){ // We start falling
                    ctx.entityBank->Speeds[e].speed_.y = 60.0f;
                    ctx.entityBank->Accelerations[e].acceleration_.y = 0;
                    ctx.entityBank->Accelerations[e].jumping_ = true;
                }
                else {
                    ctx.entityBank->Positions[e].position_.y += ctx.entityBank->Speeds[e].speed_.y * dt;
                }
                

                //if (ctx.entityBank->Accelerations[e].jumping_ &&
                //    ctx.entityBank->Positions[e].position_.y >(ctx.tileDimensions.y - 2) * ctx.tileSize) {
                //    ctx.entityBank->Positions[e].position_.y = (ctx.tileDimensions.y - 2) * ctx.tileSize;
                //    ctx.entityBank->Speeds[e].speed_.y = 0;
                //    ctx.entityBank->Accelerations[e].acceleration_.y = 0;
                //    ctx.entityBank->Accelerations[e].jumping_ = false;
                //}
            }
            else {
               // sf::Vector2f dm = ctx.entityBank->Speeds[e].speed_ * dt;
                ctx.entityBank->Positions[e].position_ += ctx.entityBank->Speeds[e].speed_ * dt;
            }
        }
    }
}

std::pair<bool,bool> PhysicsSystem::CheckCollisions(Context & ctx, Entity e1, sf::Vector2f offset)
{
    bool hitX = false, hitY = false;
    EntityBank* bank = ctx.entityBank;

    sf::FloatRect e1HitBoxX = bank->HitBoxes[e1].hitBox_;
    e1HitBoxX.left += bank->Positions[e1].position_.x + offset.x;
    e1HitBoxX.top += bank->Positions[e1].position_.y;

    sf::FloatRect e1HitBoxY = bank->HitBoxes[e1].hitBox_;
    e1HitBoxY.left += bank->Positions[e1].position_.x;
    e1HitBoxY.top += bank->Positions[e1].position_.y + offset.y;

    ComponentMask components = ComponentFlag::HitBox | ComponentFlag::Position;
    for (Entity e2 = 0; e2 < MAX_ENTITIES; e2++) {
        if (e2 == e1) {
            // Don't hit test against ourselves
            continue;
        }


        if (bank->HasComponents(e2, components)) {
            sf::FloatRect e2HitBox = bank->HitBoxes[e2].hitBox_;
            e2HitBox.left += bank->Positions[e2].position_.x;
            e2HitBox.top += bank->Positions[e2].position_.y;

            sf::FloatRect intersection;

            if (e1HitBoxX.intersects(e2HitBox, intersection)) {
                hitX = true;
            }
            if (e1HitBoxY.intersects(e2HitBox, intersection)) {
                hitY = true;
            }
        }
    }

    //Cmp::Speed& speed = ctx.entityBank->Speeds[e];
    //bool hitX, hitY;
    //hitX = false;
    //if (speed.speed_.y < 0) {
    //    hitY =  false;
    //}
    //else {
    //    hitY = true;
    //}
    return std::pair<bool,bool>(hitX, hitY);
}

std::pair<bool, bool> PhysicsSystem::CheckCollisions(Context & context, Entity entity)
{
    return CheckCollisions(context, entity, sf::Vector2f(0, 0));
}
