#pragma once
#include "Context.h"

// Base class for all systems
class System{
public:
    System(){}
    ~System(){}
    virtual void Initialize(Context& context) = 0;
    virtual void Run(Context& context) = 0;
    
    // Signatures to copy, for the lazy :)
    // void Initialize(Context& ctx) override;
    // void Run(Context& ctx) override;
};
