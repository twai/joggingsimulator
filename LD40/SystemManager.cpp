#include "SystemManager.h"
#include "SystemList.h"
#include "System.h"
#include <iostream>


SystemManager::SystemManager()
{
    // Goodbye sweet prince :( 
    // It's time for ugly hacks
    //systems_.push_back(new WorldgenSystem());
    //systems_.push_back(new HeroSystem());
    //systems_.push_back(new PhysicsSystem());
    //systems_.push_back(new MovementSystem());
    //systems_.push_back(new InputSystem());
    
    systems_.push_back(&heroSystem_);
    systems_.push_back(&worldgenSystem_);
    systems_.push_back(&physicsSystem_);
    systems_.push_back(&inputSystem_);
}


SystemManager::~SystemManager()
{
}

void SystemManager::Initialize(Context & ctx)
{
    heroSystem_.Initialize(ctx);
    worldgenSystem_.Initialize(ctx);
    physicsSystem_.Initialize(ctx);
    inputSystem_.Initialize(ctx);
}

void SystemManager::Run(Context & ctx)
{
    heroSystem_.Run(ctx);
    worldgenSystem_.Run(ctx);
    physicsSystem_.Run(ctx);
    inputSystem_.Run(ctx);
}

void SystemManager::Reset(Context & ctx){
    heroSystem_.Reset(ctx);
    worldgenSystem_.Reset(ctx);
}
