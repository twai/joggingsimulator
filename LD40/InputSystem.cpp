#include "InputSystem.h"
#include "XboxController.h"
#include <iostream>
#include "SoundBank.h"

InputSystem::InputSystem(){
}


InputSystem::~InputSystem(){
}

void InputSystem::Initialize(Context & ctx)
{
    int controllerIndex = -1;
    for (int i = 0; i < 4; i++) {
        if (XboxController::isConnected(i)) {
            controllerIndex = i;
            controller_ = i;
            break;
        }
    }
    if (controllerIndex < 0) {
        std::cerr << "No controller connected :(" << std::endl;
    }
}

void InputSystem::Run(Context & ctx)
{
    if (ctx.window->hasFocus()) {
        ComponentMask components = ComponentFlag::Input | ComponentFlag::Acceleration | ComponentFlag::Speed;
        for (Entity e = 0; e < MAX_ENTITIES; e++) {
            if (ctx.entityBank->HasComponents(e, components)) {
                int brk = 0;
                XboxController::XboxButton button = buttonsPerInstance[ctx.instance];
                sf::Keyboard::Key key = keysPerInstance[ctx.instance];
                if (XboxController::isButtonPressed(controller_, button)
                    || sf::Keyboard::isKeyPressed(key)){
                    if (ctx.entityBank->Accelerations[e].jumping_ == false) {
                        SoundBank::PlayJumpSound();
                        ctx.entityBank->Accelerations[e].jumping_ = true;
                        ctx.entityBank->Accelerations[e].acceleration_ = sf::Vector2f(0, 100);
                        ctx.entityBank->Speeds[e].speed_.y -= 60;
                    }
                }
                else if(ctx.entityBank->Speeds[e].speed_.y < 0) {
                    ctx.entityBank->Accelerations[e].acceleration_.y = 0;
                    ctx.entityBank->Speeds[e].speed_.y = 0;
                }
            }
        }
    }
}
