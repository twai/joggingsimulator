#pragma once
#include "System.h"
#include <SFML/Window.hpp>
#include "XboxController.h"

class InputSystem : public System{
public:
    InputSystem();
    ~InputSystem();
    void Initialize(Context& ctx) override;
    void Run(Context& ctx) override;

private:
    int controller_;
    XboxController::XboxButton buttonsPerInstance[8]{
        XboxController::Y,
        XboxController::X,
        XboxController::B,
        XboxController::A,
        XboxController::DPAD_UP,
        XboxController::DPAD_LEFT,
        XboxController::DPAD_RIGHT,
        XboxController::DPAD_DOWN,
    };

    sf::Keyboard::Key keysPerInstance[8]{
        sf::Keyboard::A,
        sf::Keyboard::S,
        sf::Keyboard::D,
        sf::Keyboard::F,
        sf::Keyboard::Z,
        sf::Keyboard::X,
        sf::Keyboard::C,
        sf::Keyboard::V,
    };
};
