#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include <map>
#include "AssetManager.h"

enum class AnimationName {
    Running,
    Jumping, 
    Idle
};

class AnimatedSprite
{
public:
    AnimatedSprite();
    ~AnimatedSprite();
    void AddSprite(sf::Sprite* sprite);
    void SetAnimation(AnimationName name, int firstIndex, int lastIndex, float timePerFrame);
    void SetTime(AnimationName name, float time);
    void Tick(AnimationName name, float time);
    sf::Sprite* GetSprite(AnimationName name);

private:
    struct Animation {
        Animation(){}
        Animation(int firstIndex, int lastIndex, float timePerFrame)
            : FirstIndex(firstIndex), LastIndex(lastIndex), TimePerFrame(timePerFrame), CurrentIndex(0), CurrentTime(0){
            TotalTime = (lastIndex - firstIndex) * timePerFrame;
        }
        int FirstIndex, LastIndex, CurrentIndex;
        float TimePerFrame;
        float CurrentTime;
        float TotalTime;
    };
    std::vector<sf::Sprite*> sprites_;
    std::map<AnimationName, Animation> animations_;
    float timePerFrame_;
    float currentTime_;
};

