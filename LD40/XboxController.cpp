#include "XboxController.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Xinput.h>
#include <math.h>
#include <algorithm>

#define MAX_MAGNITUDE 32767 // As per https://msdn.microsoft.com/en-us/library/windows/desktop/microsoft.directx_sdk.reference.xinput_gamepad(v=vs.85).aspx
#define MAX_MOTOR_SPEED 65535
#define MAX_TRIGGER 255
#define LEFT_DEADZONE XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE
#define RIGHT_DEADZONE XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE 


XboxController::XboxController() {
}


XboxController::~XboxController() {
}

int XboxController::MaxNumControllers()
{
    return XUSER_MAX_COUNT;
}

bool XboxController::isConnected(ControllerIndex controller)
{
    XINPUT_STATE state;
    ZeroMemory(&state, sizeof(XINPUT_STATE));
    DWORD result = XInputGetState(controller, &state);
    return ERROR_SUCCESS == result;
}

bool XboxController::isButtonPressed(ControllerIndex controller, XboxButton button)
{
    XINPUT_STATE state;
    ZeroMemory(&state, sizeof(XINPUT_STATE));
    DWORD success = XInputGetState(controller, &state);
    return (state.Gamepad.wButtons & button) == button;
}

void XboxController::getTriggers(ControllerIndex controller, float & left, float & right)
{
    XINPUT_STATE state;
    ZeroMemory(&state, sizeof(XINPUT_STATE));
    XInputGetState(controller, &state);

    BYTE LT = state.Gamepad.bLeftTrigger;
    if (LT > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) {
        left = (LT - XINPUT_GAMEPAD_TRIGGER_THRESHOLD) / static_cast<float>(MAX_TRIGGER - XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
    }
    else {
        left = 0.0f;
    }

    BYTE RT = state.Gamepad.bRightTrigger;
    if (RT > XINPUT_GAMEPAD_TRIGGER_THRESHOLD) {
        right = (RT - XINPUT_GAMEPAD_TRIGGER_THRESHOLD) / static_cast<float>(MAX_TRIGGER - XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
    }
    else {
        right = 0.0f;
    }
}

void XboxController::getStickPosition(ControllerIndex controller, sf::Vector2f & leftDir, float& leftMagnitude, sf::Vector2f & rightDir, float& rightMagnitude)
{
    XINPUT_STATE state;
    ZeroMemory(&state, sizeof(XINPUT_STATE));
    DWORD success = XInputGetState(controller, &state);

    // Left thumbstick
    float LX = state.Gamepad.sThumbLX;
    float LY = state.Gamepad.sThumbLY;

    float magnitude = sqrt(LX * LX + LY * LY);

    leftDir.x = LX / magnitude;
    leftDir.y = -(LY / magnitude);

    if (magnitude > LEFT_DEADZONE) {
        if (magnitude > MAX_MAGNITUDE) {
            magnitude = MAX_MAGNITUDE;
        }
        magnitude -= LEFT_DEADZONE;
        leftMagnitude = magnitude / (MAX_MAGNITUDE - LEFT_DEADZONE);
    }
    else { // Deadzone
        leftMagnitude = 0.0f;
    }

    // Right thumbstick
    float RX = state.Gamepad.sThumbRX;
    float RY = state.Gamepad.sThumbRY;

    magnitude = sqrt(RX * RX + RY * RY);

    rightDir.x = RX / magnitude;
    rightDir.y = -(RY / magnitude);

    if (magnitude > RIGHT_DEADZONE) {
        if (magnitude > MAX_MAGNITUDE) {
            magnitude = MAX_MAGNITUDE;
        }
        magnitude -= RIGHT_DEADZONE;
        rightMagnitude = magnitude / (MAX_MAGNITUDE - RIGHT_DEADZONE);
    }
    else { // Deadzone
        rightMagnitude = 0.0f;
    }
}


/// Specify motor speeds in the 0.0-1.0 range
/// Note that leftMotor is low-frequency while rightMotor is highFrequency. Experiment!
void XboxController::setVibration(ControllerIndex controller, float leftMotor, float rightMotor)
{
    float clampedLeft = max(0.0f, min(1.0f, leftMotor));
    float clampedRight = max(0.0f, min(1.0f, rightMotor));

    XINPUT_VIBRATION vibration;
    ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
    vibration.wLeftMotorSpeed = clampedLeft * MAX_MOTOR_SPEED;
    vibration.wRightMotorSpeed = clampedRight * MAX_MOTOR_SPEED;
    XInputSetState(controller, &vibration);
}
