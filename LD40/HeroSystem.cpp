#include "HeroSystem.h"
#include "Strings.h"
#include "EntityFactory.h"
#include <iostream>
#include "SoundBank.h"

HeroSystem::HeroSystem(){
}


HeroSystem::~HeroSystem(){
}

void HeroSystem::Initialize(Context & ctx)
{
    ctx.assetManager->LoadTexture(Strings::CharSpritesheetPath); 
    for (int nHeroes = 0; nHeroes < heroVariations; nHeroes++) {
        for (int i = 0; i < 9; i++) {
            sf::IntRect rect = sf::IntRect(16 * i, 16 * nHeroes, 16, 16);
            SpriteHandle handle = ctx.assetManager->CreateSprite(Strings::CharSpritesheetPath, rect);
            //heroAnimation_.AddSprite(ctx.assetManager->GetSprite(handle));
            heroAnimations_[nHeroes].AddSprite(ctx.assetManager->GetSprite(handle));
        }

        heroAnimations_[nHeroes].SetAnimation(AnimationName::Running, 0, 7, 10.f);
        heroAnimations_[nHeroes].SetAnimation(AnimationName::Idle, 8, 8, 10.f);
        heroAnimations_[nHeroes].Tick(AnimationName::Idle, 0);
    }
    
    currentHero = rand() % heroVariations; 
    Entity e = EntityFactory::Generate(ctx, EntityBlueprint::Hero);
    hero_ = e;
    ctx.entityBank->Sprites[e].handle_ = 0;
    ctx.entityBank->Sprites[e].sprite_ = heroAnimations_[currentHero].GetSprite(AnimationName::Running);
    ctx.entityBank->Sprites[e].layer_ = 3;
    ctx.entityBank->Positions[e].position_ = sf::Vector2f(100, 16);
    ctx.entityBank->HitBoxes[e].hitBox_ = sf::FloatRect(6, 0, 4, 16);
    //ctx.entityBank->Accelerations[e].acceleration_ = sf::Vector2f(0, -400);
    ctx.entityBank->Speeds[e].speed_ = sf::Vector2f(-ctx.scrollSpeed, 0);
    if (!ctx.active) {
        ctx.entityBank->RemoveComponents(e, ComponentFlag::Sprite);
    }


}

void HeroSystem::Run(Context & ctx)
{
    if (ctx.active) {
        accumulatedTime_ += ctx.dt;
        float animTick = -ctx.scrollSpeed * ctx.dt;
        heroAnimations_[currentHero].Tick(AnimationName::Running, animTick);
        float respawnHeight = static_cast<float>((ctx.tileDimensions.y + 5) * ctx.tileSize);
        EntityBank* bank = ctx.entityBank;

        ComponentMask components = ComponentFlag::Hero | ComponentFlag::Position | ComponentFlag::Sprite | ComponentFlag::Acceleration;
        for (Entity e = 0; e < MAX_ENTITIES; e++) {
            if (bank->HasComponents(e, components)) {
                ctx.entityBank->Sprites[e].sprite_ = heroAnimations_[currentHero].GetSprite(AnimationName::Running);
                ctx.entityBank->Speeds[e].speed_.x = -ctx.scrollSpeed;
                if (bank->Positions[e].position_.y > respawnHeight) {
                    ctx.scrollSpeed = 0;
                    if (!ctx.heroDied) {
                        SoundBank::PlayDeathSound();
                    }
                    ctx.heroDied = true;
                }
            }

            if (bank->HasComponents(e, ComponentFlag::SilentHitBox | ComponentFlag::Position)) {
                if (bank->SilentHitBoxes[e].usage_ == Cmp::SilentHitBox::Hero) {
                    if (bank->Positions->position_.x < (-ctx.tileSize)) {
                        bank->DestroyEntity(e);
                    }
                    else if (CollidesWithHero(ctx, e)) {
                            ctx.charges++;
                            ctx.nextHeroOut = bank->SilentHitBoxes[e].index_;
                            bank->DestroyEntity(e);
                            SoundBank::PlayRecruitSound();
                        }

                }
                else if (bank->SilentHitBoxes[e].usage_ == Cmp::SilentHitBox::HeroMarker) {
                    // Fill in hero info!
                    bank->SilentHitBoxes[e].usage_ = Cmp::SilentHitBox::Hero;
                    bank->SilentHitBoxes[e].hitBox_ = sf::FloatRect(6, 0, 4, 16);
                    bank->AddComponents(e, ComponentFlag::Sprite);
                    int id = rand() % heroVariations;
                    bank->Sprites[e].sprite_ = heroAnimations_[id].GetSprite(AnimationName::Idle);
                    bank->Sprites[e].layer_ = 2;
                    bank->SilentHitBoxes[e].index_ = id;
                }
            }
        }
    }
}

void HeroSystem::Reset(Context & ctx)
{
    ctx.heroDied = false;
    ctx.scrollSpeed = ctx.originalScrollSpeed;
    currentHero = ctx.nextHeroIn;
    heroAnimations_[currentHero].SetTime(AnimationName::Running, 0.0f);
    Respawn(ctx, hero_);

    // Remove unclaimed heroes
    for (Entity e = 0; e < MAX_ENTITIES; e++) {
        if (ctx.entityBank->HasComponents(e, ComponentFlag::SilentHitBox | ComponentFlag::Position)) {
            if(ctx.entityBank->SilentHitBoxes[e].usage_ == Cmp::SilentHitBox::Hero ||
                ctx.entityBank->SilentHitBoxes[e].usage_ == Cmp::SilentHitBox::HeroMarker)
                ctx.entityBank->DestroyEntity(e);
        }
    }
}

bool HeroSystem::CollidesWithHero(Context & ctx, Entity e)
{
    sf::FloatRect first = ctx.entityBank->SilentHitBoxes[e].hitBox_;
    first.left += ctx.entityBank->Positions[e].position_.x;
    first.top += ctx.entityBank->Positions[e].position_.y;

    sf::FloatRect second = ctx.entityBank->HitBoxes[hero_].hitBox_;
    second.left += ctx.entityBank->Positions[hero_].position_.x;
    second.top += ctx.entityBank->Positions[hero_].position_.y;

    return first.intersects(second);
}

void HeroSystem::Respawn(Context & ctx, Entity e)
{
    ctx.entityBank->AddComponents(e, ComponentFlag::Sprite);
    ctx.entityBank->Positions[e].position_ = sf::Vector2f(100, 16);
    ctx.entityBank->Speeds[e].speed_ = sf::Vector2f(-ctx.scrollSpeed, 0);
    ctx.entityBank->Accelerations[e].acceleration_ = sf::Vector2f(0, 0);
    ctx.entityBank->Accelerations[e].jumping_ = true;
    ctx.entityBank->Speeds[e].speed_ = sf::Vector2f(0, 0);
}
