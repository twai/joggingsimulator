#pragma once
#include <string>
class Strings {
public:
    static const std::string SpritesheetPath;
    static const std::string CharSpritesheetPath;
    static const std::string GUIBorderPath;
    static const std::string GUIBlindsPath;
    static const std::string SoundJumpPath;
    static const std::string SoundRecruitPath;
    static const std::string SoundDeathPath;
};