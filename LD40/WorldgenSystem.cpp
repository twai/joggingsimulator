﻿#include "WorldgenSystem.h"
#include "Strings.h"
#include "EntityFactory.h"
//#include <random>
#include <iostream>
#define HERO_MARKER_CHANCE 150

WorldgenSystem::WorldgenSystem() {
}


WorldgenSystem::~WorldgenSystem() {
}

void WorldgenSystem::Reset(Context & ctx)
{
    EntityBank* entities = ctx.entityBank;
    ComponentMask c = ComponentFlag::Scrolling;
    for (Entity e = 0; e < MAX_ENTITIES; e++) {
        if(entities->ScrollingTiles[e].type_ == Cmp::ScrollingTile::Ground){
            if (!entities->HasComponents(e, ComponentFlag::HitBox)) {
                GenerateGroundTile(entities->Sprites[e], true);
                ctx.entityBank->ScrollingTiles[e].type_ = Cmp::ScrollingTile::Ground;
                ctx.entityBank->AddComponents(e, ComponentFlag::HitBox);
                ctx.entityBank->HitBoxes[e].hitBox_ = sf::FloatRect(0, 3, 16, 13);
            }
        }
    }
}


void WorldgenSystem::Initialize(Context & ctx)
{
    int ts = ctx.tileSize;

    // Normally we'd load this from a file, but game jam and all that  ¯\_(ツ)_/¯

    // There's 3 sky tiles that start at (16,0)
    for (int i = 0; i < 3; i++) {
        skySpriteHandles[i] = ctx.assetManager->CreateSprite(Strings::SpritesheetPath, sf::IntRect(ts * (i + 1), 0, ts, ts));
        skySprites[i] = ctx.assetManager->GetSprite(skySpriteHandles[i]);
    }
    // There's 2 ground tiles that start at (64,0)
    for (int i = 0; i < 2; i++) {
        groundSpriteHandles[i] = ctx.assetManager->CreateSprite(Strings::SpritesheetPath, sf::IntRect(ts * (i + 4), 0, ts, ts));
        groundSprites[i] = ctx.assetManager->GetSprite(groundSpriteHandles[i]);
    }

    // There's 2 prop tiles that start at (96,0)
    for (int i = 0; i < 2; i++) {
        propSpriteHandles[i] = ctx.assetManager->CreateSprite(Strings::SpritesheetPath, sf::IntRect(ts * (i + 6), 0, ts, ts));
        propSprites[i] = ctx.assetManager->GetSprite(propSpriteHandles[i]);
    }

    for (int y = 0; y < ctx.tileDimensions.y; y++) {
        for (int x = 0; x < ctx.tileDimensions.x + 2; x++) {
            Entity e = EntityFactory::Generate(ctx, EntityBlueprint::BackgroundTile);
            if (e == NO_ENTITY) {
                // Oh no!
                return;
            }
            Cmp::Position& pos = ctx.entityBank->GetPosition(e);
            pos.position_ = sf::Vector2f(static_cast<float>(ts * x), static_cast<float>(ts * y));

            Cmp::Sprite& sprite = ctx.entityBank->GetSprite(e);

            if (y + 1 == ctx.tileDimensions.y) { // Bottom row = ground
                if (GenerateGroundTile(sprite, true)) {
                    ctx.entityBank->AddComponents(e, ComponentFlag::HitBox);
                    ctx.entityBank->HitBoxes[e].hitBox_ = sf::FloatRect(0, 3, 16, 13);
                }
                ctx.entityBank->ScrollingTiles[e].type_ = Cmp::ScrollingTile::Ground;
            }
            else { // Sky
                GenerateSkyTile(sprite);
                ctx.entityBank->ScrollingTiles[e].type_ = Cmp::ScrollingTile::Sky;

                // prop
                if (y == 2 && rand()% 5 == 0){
                    Entity e2 = EntityFactory::Generate(ctx, EntityBlueprint::BackgroundTile);
                    ctx.entityBank->Positions[e2].position_ = sf::Vector2f(ctx.entityBank->Positions[e].position_);
                    ctx.entityBank->Sprites[e2].layer_ = 1;
                    GeneratePropTile(ctx.entityBank->Sprites[e2]);
                    ctx.entityBank->Speeds[e2].speed_ = sf::Vector2f(0, 0);
                    ctx.entityBank->ScrollingTiles[e2].type_ = Cmp::ScrollingTile::Prop;
                }
            }


            ctx.entityBank->Speeds[e].speed_ = sf::Vector2f(0, 0);
            tiles_.push_back(e);
        }
    }
}

void WorldgenSystem::Run(Context & ctx)
{
    ComponentMask c = ComponentFlag::Scrolling | ComponentFlag::Position | ComponentFlag::Sprite;
    for (Entity e = 0; e < MAX_ENTITIES; e++) {
        if (ctx.entityBank->HasComponents(e, c)) {
            if (ctx.entityBank->Positions[e].position_.x < -ctx.tileSize) {
                ctx.entityBank->Positions[e].position_.x = (ctx.tileSize * (ctx.tileDimensions.x + 2)) + ctx.entityBank->Positions[e].position_.x;

                if (ctx.entityBank->ScrollingTiles[e].type_ == Cmp::ScrollingTile::Ground)
                {
                    // Ground tile
                    bool ground = GenerateGroundTile(ctx.entityBank->Sprites[e]);
                    if (ground) {
                        ctx.metersTraveled += 1;
                        ctx.entityBank->AddComponents(e, ComponentFlag::HitBox);
                        ctx.entityBank->HitBoxes[e].hitBox_ = sf::FloatRect(0, 3, 16, 13);

                        // See if we want a prop on top 
                        if (ground && ctx.entityBank->Positions[e].position_.y > (ctx.tileSize * 2) && rand() % 5 == 0) {
                            Entity e2 = EntityFactory::Generate(ctx, EntityBlueprint::BackgroundTile);
                            ctx.entityBank->Positions[e2].position_.x = ctx.entityBank->Positions[e].position_.x;
                            ctx.entityBank->Positions[e2].position_.y = ctx.entityBank->Positions[e].position_.y - ctx.tileSize;
                            ctx.entityBank->Sprites[e2].layer_ = 1;
                            GeneratePropTile(ctx.entityBank->Sprites[e2]);
                            ctx.entityBank->Speeds[e2].speed_ = sf::Vector2f(0, 0);
                            ctx.entityBank->ScrollingTiles[e2].type_ = Cmp::ScrollingTile::Prop;
                        }
                        
                        // See if we want to spawn a hero marker here
                        //std::cout << "Spawn chance: 1 in " << HERO_MARKER_CHANCE - ctx.spawnBias << std::endl;
                        if (rand() % (HERO_MARKER_CHANCE-ctx.spawnBias) == 0) {
                          //  std::cout << "Got it!";
                            ctx.spawnBias = 0;
                            GenerateHeroMarker(ctx, ctx.entityBank->Positions[e].position_);
                        }
                        else {
                            ctx.spawnBias++;
                        }
                    }                
                    else { // Hole
                        ctx.entityBank->RemoveComponents(e, ComponentFlag::HitBox);
                    }


                }
                else if (ctx.entityBank->ScrollingTiles[e].type_ == Cmp::ScrollingTile::Sky) {
                    // Sky tile
                    GenerateSkyTile(ctx.entityBank->Sprites[e]);
                }
                else if (ctx.entityBank->ScrollingTiles[e].type_ == Cmp::ScrollingTile::Prop) {
                    ctx.entityBank->DestroyEntity(e);
                }
            }
        }
    }
}


void WorldgenSystem::GenerateSkyTile(Cmp::Sprite& sprite)
{
    // Get some randomly generated clouds!
    int r = rand() % 100; // 0-99
    int spriteIndex = 0;
    if (r < 5) {
        spriteIndex = 1;
    }
    else if (r < 10) {
        spriteIndex = 2;
    }
    sprite.handle_ = skySpriteHandles[spriteIndex];
    sprite.sprite_ = skySprites[spriteIndex];
    sprite.layer_ = 0;
}

// Returns true if a real ground tile was created
bool WorldgenSystem::GenerateGroundTile(Cmp::Sprite& sprite) {
    return GenerateGroundTile(sprite, false);
}

bool WorldgenSystem::GenerateGroundTile(Cmp::Sprite& sprite, bool force)
{
    bool ground = true;
    int r = rand() % 100; // 0-99
    int spriteIndex = 0;
    if (r < 30) {
        spriteIndex = 1;
    }

    r = rand() % 25;
    if (!force && r < 1) {
        sprite.handle_ = skySpriteHandles[0];
        sprite.sprite_ = skySprites[0];
        sprite.layer_ = 0;
        sprite.debugName_ = "Sky";
        ground = false;
    }
    else {
        sprite.handle_ = groundSpriteHandles[spriteIndex];
        sprite.sprite_ = groundSprites[spriteIndex];
        sprite.layer_ = 0;
        sprite.debugName_ = "Ground";
    }
    return ground;
}

void WorldgenSystem::GeneratePropTile(Cmp::Sprite & sprite)
{
    int r = rand() % 100; // 0-99
    int spriteIndex = 0;
    if (r < 30) {
        spriteIndex = 1;
    }

    sprite.handle_ = propSpriteHandles[spriteIndex];
    sprite.sprite_ = propSprites[spriteIndex];
    sprite.layer_ = 1;
}

void WorldgenSystem::GenerateHeroMarker(Context& ctx, const sf::Vector2f& tilePos)
{
    Entity e = ctx.entityBank->CreateEntity(ComponentFlag::SilentHitBox | ComponentFlag::Position | ComponentFlag::WorldEntity);
    ctx.entityBank->SilentHitBoxes[e].usage_ = Cmp::SilentHitBox::HeroMarker;
    ctx.entityBank->Positions[e].position_ = tilePos + sf::Vector2f(0.0f, static_cast<float>(-ctx.tileSize + 1));
}
