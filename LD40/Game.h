#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Context.h"
#include "AssetManager.h"
#include "SystemManager.h"
#define GLOBAL_SCALE 3
#define TILE_SIZE 16
#include "RenderSystem.h"
#include "Timer.h"
#include "GuiManager.h"
#include <vector>

struct GameInstance {
    Context context_;
    SystemManager systems_;
    EntityBank entityBank_;
    int instanceId_;
};

class Game{
public:
    Game();
    ~Game();
    bool Initialize();
    bool Update();
    void Render();
    bool ErrorState = false;
private:
    int numGames = 4;
    void DelayedInitialize();
    sf::RenderWindow window_;
    int window_w_, window_h_;
    AssetManager assetManager_;
    // sf::Clock timer_;
   // GameInstance games[NUM_GAMES];
    std::vector<GameInstance*> games_;
     
    Timer timer_;
    RenderSystem renderer_;
    GuiManager guiManager_;
    bool prevStart_= false;
    bool prevUp_ = false;
    bool prevDown_ = false;
    int spawnBias_ = 0;
    sf::Text* scoreText_;
    sf::Text* menuText_;
    sf::Text* gameOverText_;
    bool init_ = false;
};