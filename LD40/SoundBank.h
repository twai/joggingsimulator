#pragma once
#include <SFML/Audio.hpp>

class SoundBank{
public:
    static bool Initialize();
    SoundBank();
    ~SoundBank();
    static void PlayJumpSound();
    static void PlayRecruitSound();
    static void PlayDeathSound();
private:
    static sf::SoundBuffer bJumpSound_;
    static sf::Sound jumpSound_;
    static sf::SoundBuffer bRecruitSound_;
    static sf::Sound recruitSound_;
    static sf::SoundBuffer bDeathSound_;
    static sf::Sound deathSound_;
};

