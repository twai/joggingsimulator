// Class for generating common entities
#pragma once
#include "Components.h"
#include "Context.h"

enum class EntityBlueprint {
    BackgroundTile,
    Hero,
};

class EntityFactory
{
public:
    EntityFactory();
    ~EntityFactory();
    static Entity Generate(Context& context, EntityBlueprint blueprint);

private:
};

