#include "EntityFactory.h"



EntityFactory::EntityFactory()
{
}


EntityFactory::~EntityFactory()
{
}

Entity EntityFactory::Generate(Context & ctx, EntityBlueprint blueprint)
{
    switch (blueprint) {
    case EntityBlueprint::BackgroundTile:
    {
        ComponentMask components = Sprite | Position | Scrolling | WorldEntity;
        return ctx.entityBank->CreateEntity(components);
    }
    break;

    case EntityBlueprint::Hero:
    {
        ComponentMask components = Sprite | Position | Input | Acceleration | Speed | Hero | WorldEntity | HitBox;
        return ctx.entityBank->CreateEntity(components);
    }
    break;

    default:
        return 0;
    }
}
