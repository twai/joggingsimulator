#pragma once
#include "System.h"
#include "AnimatedSprite.h"

class HeroSystem : public System{
public:
    HeroSystem();
    ~HeroSystem();
    void Initialize(Context& ctx) override;
    void Run(Context& ctx) override;
    void Reset(Context& ctx);

private:
    bool CollidesWithHero(Context& ctx, Entity e);
    static const int heroVariations = 4;
    void Respawn(Context& ctx, Entity e);
    AnimatedSprite heroAnimations_[heroVariations];
    // AnimatedSprite heroAnimation_;
    Entity hero_;
    float accumulatedTime_ = 0;
    int currentHero = 0;
    int nextHeroIndex;
};
