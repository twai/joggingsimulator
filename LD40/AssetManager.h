#pragma once
#include <string>
#include <SFML/Graphics.hpp>
#include <unordered_map>

using SpriteHandle = int;
#define MAX_SPRITES 1000

class AssetManager
{
public:
    AssetManager();
    ~AssetManager();
    bool Initialize();
    bool LoadTexture(std::string path);
    sf::Texture& GetSpriteSheet() { return spriteSheet_; }
    SpriteHandle CreateSprite(const sf::Texture& texture);
    SpriteHandle CreateSprite(const sf::Texture& texture, sf::IntRect area);
    SpriteHandle CreateSprite(const std::string textureHandle);
    SpriteHandle CreateSprite(const std::string textureHandle, sf::IntRect area);
    sf::Sprite* GetSprite(SpriteHandle handle);
    void DestroySprite(SpriteHandle handle);
    static sf::Sprite* EmptySprite() { return emptySprite_; }
    static sf::Sprite* BorderSprite() { return borderSprite_; }
    static sf::Sprite* BlindsSprite() { return blindsSprite_; }
private:
    struct SpriteSlot {
        bool IsFree = true;
        sf::Sprite* Sprite;
    };
    static sf::Sprite* emptySprite_;
    static sf::Sprite* borderSprite_;
    static sf::Sprite* blindsSprite_;

    const SpriteHandle emptySpriteHandle = 0;
    sf::Texture spriteSheet_;
    SpriteSlot sprites_[MAX_SPRITES];
    std::unordered_map<std::string, sf::Texture> textures_;
};
