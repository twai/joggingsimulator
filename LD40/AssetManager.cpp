#include "AssetManager.h"
#include "Strings.h"
#include <iostream>

sf::Sprite* AssetManager::emptySprite_ = nullptr;
sf::Sprite* AssetManager::borderSprite_ = nullptr;
sf::Sprite* AssetManager::blindsSprite_ = nullptr;

AssetManager::AssetManager()
{
}


AssetManager::~AssetManager()
{
    // Delete all dynamically allocated sprites
    for (SpriteHandle i = 0; i < MAX_SPRITES; i++) {
        if (!sprites_[i].IsFree) {
            delete sprites_[i].Sprite;
        }
    }
}

bool AssetManager::Initialize()
{
    // Preload all the textures we know we'll need
    if (!LoadTexture(Strings::SpritesheetPath)) {
        return false;
    }

    // Create a white sprite to use when sprite fetches fail
    sf::Image img;
    img.create(8, 8, sf::Color::White);
    
    if (!textures_["WHITE_TEX"].loadFromImage(img)) {
        return false;
    }
    sprites_[emptySpriteHandle].Sprite = new sf::Sprite(textures_["WHITE_TEX"]);
    sprites_[emptySpriteHandle].IsFree = false;

    if (!emptySprite_) {
        emptySprite_ = sprites_[emptySpriteHandle].Sprite;
    }

    if (!borderSprite_) {
        LoadTexture(Strings::GUIBorderPath);
        SpriteHandle h = CreateSprite(Strings::GUIBorderPath);
        borderSprite_ = GetSprite(h);
    }
    if (!blindsSprite_) {
        LoadTexture(Strings::GUIBlindsPath);
        SpriteHandle h = CreateSprite(Strings::GUIBlindsPath);
        blindsSprite_ = GetSprite(h);
    }

    return true;
}

bool AssetManager::LoadTexture(std::string path)
{
    auto it = textures_.find(path);
    if (it != textures_.end()) {
        // This texture is already loaded
        return false;
    }
    else {
        textures_[path] = sf::Texture();
        return textures_[path].loadFromFile(path);
    }
}

SpriteHandle AssetManager::CreateSprite(const sf::Texture & texture)
{
    for (SpriteHandle i = 0; i < MAX_SPRITES; i++) {
        if (sprites_[i].IsFree) {
            sprites_[i].IsFree = false;
            sprites_[i].Sprite = new sf::Sprite(texture);
            return i;
        }
    }

    // If no free sprite handle was found, return an empty sprite and print an error
    std::cerr << "Out of sprite slots! Can't load more sprites." << std::endl;
    return emptySpriteHandle;
}

SpriteHandle AssetManager::CreateSprite(const sf::Texture & texture, sf::IntRect area)
{
    for (SpriteHandle i = 0; i < MAX_SPRITES; i++) {
        if (sprites_[i].IsFree) {
            sprites_[i].IsFree = false;
            sprites_[i].Sprite = new sf::Sprite(texture, area);
            return i;
        }
    }

    // If no free sprite handle was found, return an empty sprite and print an error
    std::cerr << "Out of sprite slots! Can't load more sprites." << std::endl;
    return emptySpriteHandle;
}

SpriteHandle AssetManager::CreateSprite(const std::string textureHandle)
{
    auto it = textures_.find(textureHandle);
    // If we couldn't find texture, return an "empty" sprite and print an error
    if (it == textures_.end()) {
        std::cerr << "Tried to load a sprite from an unknown texture. Handle: " << textureHandle << std::endl;
        return emptySpriteHandle;
    }
    // Texture found, create the sprite
    else {
        return CreateSprite(it->second);
    }
}

SpriteHandle AssetManager::CreateSprite(const std::string textureHandle, sf::IntRect area)
{
    auto it = textures_.find(textureHandle);
    // If we couldn't find texture, return an "empty" sprite and print an error
    if (it == textures_.end()) {
        std::cerr << "Tried to load a sprite from an unknown texture. Handle: " << textureHandle << std::endl;
        return emptySpriteHandle;
    }
    // Texture found, create the sprite
    else {
        return CreateSprite(it->second, area);
    }
}

sf::Sprite* AssetManager::GetSprite(SpriteHandle handle)
{
    if (handle < MAX_SPRITES && !sprites_[handle].IsFree) {
        return sprites_[handle].Sprite;
    }

    return sprites_[emptySpriteHandle].Sprite;
}

void AssetManager::DestroySprite(SpriteHandle handle)
{
    if (handle < MAX_SPRITES && !sprites_[handle].IsFree) {
        delete sprites_[handle].Sprite;
        sprites_[handle].IsFree = true;
    }
}
