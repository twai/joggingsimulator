#pragma once

#include <SFML/System/Vector2.hpp>

class XboxController {
public:
    enum XboxButton {
        DPAD_UP = 0x0001,
        DPAD_DOWN = 0x0002,
        DPAD_LEFT = 0x0004,
        DPAD_RIGHT = 0x0008,
        START = 0x0010,
        BACK = 0x0020,
        LEFT_THUMB = 0x0040,
        RIGHT_THUMB = 0x0080,
        LEFT_SHOULDER = 0x0100,
        RIGHT_SHOULDER = 0x0200,
        A = 0x1000,
        B = 0x2000,
        X = 0x4000,
        Y = 0x8000
    };

    XboxController();
    ~XboxController();
    using ControllerIndex = unsigned int;
    static const int ButtonCount = 14;
    static int MaxNumControllers();
    static bool isConnected(ControllerIndex controller);
    static bool isButtonPressed(ControllerIndex controller, XboxButton button);
    static void getTriggers(ControllerIndex controller, float& left, float& right);
    static void getStickPosition(ControllerIndex controller, sf::Vector2f & leftDir, float& leftMagnitude, sf::Vector2f & rightDir, float& rightMagnitude);
    static void setVibration(ControllerIndex controller, float leftMotor, float rightMotor);

};
