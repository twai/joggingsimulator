#include "SoundBank.h"
#include "Strings.h"

sf::SoundBuffer SoundBank::bJumpSound_;
sf::Sound SoundBank::jumpSound_;
sf::SoundBuffer SoundBank::bRecruitSound_;
sf::Sound SoundBank::recruitSound_;
sf::SoundBuffer SoundBank::bDeathSound_;
sf::Sound SoundBank::deathSound_;

bool SoundBank::Initialize()
{
    if (!bJumpSound_.loadFromFile(Strings::SoundJumpPath)) {
        return false;
    }
    jumpSound_.setBuffer(bJumpSound_);

    if (!bRecruitSound_.loadFromFile(Strings::SoundRecruitPath)) {
        return false;
    }
    recruitSound_.setBuffer(bRecruitSound_);

    if (!bDeathSound_.loadFromFile(Strings::SoundDeathPath)) {
        return false;
    }
    deathSound_.setBuffer(bDeathSound_);

    return true;
}

SoundBank::SoundBank(){
}


SoundBank::~SoundBank(){
}

void SoundBank::PlayJumpSound()
{
    jumpSound_.play();
}

void SoundBank::PlayRecruitSound()
{
    recruitSound_.play();
}

void SoundBank::PlayDeathSound()
{
    deathSound_.play();
}
