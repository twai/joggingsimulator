#include "Timer.h"

using namespace std::chrono;

Timer::Timer(){
    start_time_ = high_resolution_clock::now();
}


Timer::~Timer(){
}

void Timer::Start() {
    elapsed_ = 0.0;
    start_time_ = high_resolution_clock::now();
    running_ = true;
}

double Timer::Restart(){
    Stop();
    double e = elapsed_;
    Start();
    return e;
}

void Timer::Resume(){
    start_time_ = high_resolution_clock::now();
    running_ = true;
}

double Timer::Stop(){
    high_resolution_clock::time_point t = high_resolution_clock::now();
    elapsed_ += duration_cast<duration<double>>(t - start_time_).count();
    running_ = false;
    return elapsed_;
}

double Timer::Get() const{
    if (running_) {
        high_resolution_clock::time_point t = high_resolution_clock::now();
        return duration_cast<duration<double>>(t - start_time_).count();
    }
    else {
        return elapsed_;
    }
}

void Timer::Clear(){
    running_ = false;
    elapsed_ = 0.0;
}

void Timer::ResetIfPassed(double seconds){
    if (Get() > seconds) {
        Clear();
    }
}