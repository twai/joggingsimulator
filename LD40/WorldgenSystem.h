#pragma once
#include "System.h"
#include "Components.h"
#include <vector>

class WorldgenSystem : public System{
public:
    WorldgenSystem();
    ~WorldgenSystem();
    void Initialize(Context& ctx) override;
    void Run(Context& ctx) override;
    void Reset(Context& ctx);

private:
    void GenerateSkyTile(Cmp::Sprite& sprite);
    bool GenerateGroundTile(Cmp::Sprite& sprite);
    bool GenerateGroundTile(Cmp::Sprite& sprite, bool force);
    void GeneratePropTile(Cmp::Sprite& sprite);
    void GenerateHeroMarker(Context& ctx, const sf::Vector2f& tilePos);
    //void GeneratePropTile();
    std::vector<Entity> tiles_;
    Entity skyTiles_[3];
    float dt_sum_ = 0.0f;


    SpriteHandle skySpriteHandles[3];
    sf::Sprite* skySprites[3];
    SpriteHandle groundSpriteHandles[2];
    sf::Sprite* groundSprites[2];
    SpriteHandle propSpriteHandles[2];
    sf::Sprite* propSprites[2];
};
