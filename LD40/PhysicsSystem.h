#pragma once
#include "System.h"
#include <utility>
#include "SFML/Graphics.hpp"

class PhysicsSystem : public System{
public:
    PhysicsSystem();
    ~PhysicsSystem();
    void Initialize(Context& ctx) override;
    void Run(Context& context) override;
private:
    // x and y
    std::pair<bool,bool> CheckCollisions(Context& context, Entity entity);
    std::pair<bool, bool> CheckCollisions(Context& context, Entity entity, sf::Vector2f offset);
    float accumulated_dt_ = 0.0f;
    
};

