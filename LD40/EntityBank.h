#pragma once
#define MAX_ENTITIES 150
#define NO_ENTITY 0
#include "Components.h"

class EntityBank
{
public:
    EntityBank();
    ~EntityBank();

    Entity CreateEntity(ComponentMask components);
    void DestroyEntity(Entity entity);
    bool HasComponents(Entity entity, ComponentMask components);
    void AddComponents(Entity entity, ComponentMask components);
    void RemoveComponents(Entity entity, ComponentMask components);

    Cmp::Sprite& GetSprite(Entity e) { return Sprites[e]; }
    Cmp::Position& GetPosition(Entity e) { return Positions[e]; }
    int GetHighestEntity() const { return highest_; }

    Cmp::Parent Parents[MAX_ENTITIES];
    Cmp::Position Positions[MAX_ENTITIES];
    Cmp::Sprite Sprites[MAX_ENTITIES];
    Cmp::Input Inputs[MAX_ENTITIES];
    Cmp::Speed Speeds[MAX_ENTITIES];
    Cmp::Acceleration Accelerations[MAX_ENTITIES];
    Cmp::ScrollingTile ScrollingTiles[MAX_ENTITIES];
    Cmp::HitBox HitBoxes[MAX_ENTITIES];
    Cmp::SilentHitBox SilentHitBoxes[MAX_ENTITIES];

    static const double MbUsed;

    bool CapBreached() { return capBreached_; }
private:
    ComponentMask entities_[MAX_ENTITIES] = { 0 };
    bool capBreached_ = false;
    int highest_;
};

