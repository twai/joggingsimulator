#pragma once
#include <vector>
#include "Context.h"
#include "SystemList.h"

//class System;
//class WorldgenSystem;
//class PhysicsSystem;
//class HeroSystem;
//class MovementSystem;
//class InputSystem;

class SystemManager
{
public:
    SystemManager();
    ~SystemManager();
    void Initialize(Context& context);
    void Run(Context& context);
    void Reset(Context& context);

private:
    std::vector<System*> systems_;
    WorldgenSystem worldgenSystem_;
    HeroSystem heroSystem_;
    PhysicsSystem physicsSystem_;
    InputSystem inputSystem_;
};

